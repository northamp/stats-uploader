set(BLREVIVE_VERSION "1.0.0-beta.1" CACHE STRING "version of the blrevive library")

CPMAddPackage(
    NAME BLRevive
    VERSION ${BLREVIVE_VERSION}
    GIT_REPOSITORY https://gitlab.com/blrevive/blrevive
    GIT_TAG v${BLREVIVE_VERSION}
)

CPMAddPackage(
    NAME libcurl
    VERSION 8.2.1
    GIT_REPOSITORY "https://github.com/curl/curl.git"
	GIT_TAG curl-8_2_1
	OPTIONS
	    "CURL_USE_SCHANNEL ON"
        "CMAKE_USE_LIBSSH2 OFF"
        "BUILD_CURL_EXE OFF"
        "BUILD_SHARED_LIBS OFF"
        "HTTP_ONLY ON"
        "BUILD_TESTING OFF"
)