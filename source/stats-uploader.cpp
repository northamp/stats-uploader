#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <SdkHeaders.h>
#include <stats-uploader/stats-uploader.h>

#include <fstream>
#include <sstream>
#include <curl/curl.h>

using namespace BLRE;

bool EndGameCallback(UObject* Object, FFrame& Stack, void* const Result) {
	Log->debug("Fired EndGameCallback");
	AFoxGame* game = (AFoxGame*)Object;
	UploaderConfig uploaderConfig = uploaderConfigFromFile();
	nlohmann::json endgameJson = getEndgameJson(game, uploaderConfig.Game);
	std::thread uploadThread(uploadLeaderboard, game, uploaderConfig, endgameJson);
	uploadThread.detach();

	return false;
}

/**
 * Initialization of the module. 
 * 
 * @remark **Do not change the name or remove the __declspec(dllexport) from this function, otherwise BLRevive will fail to load the module!**
 * 
 * @param blre pointer to BLRevive API
*/
extern "C" __declspec(dllexport) void InitializeModule(BLRevive *blre)
{
    if (!Utils::IsServer()) {
        return;
    }

    // safe reference to blrevive api
    BLReviveAPI = blre;

    // create a logger instance for this module
    Log = blre->LogFactory->Get("stats-uploader");
    Log->info("Initializing stats-uploader");

	blre->FunctionDetour->HookPre(52950, EndGameCallback); // FoxGame.FoxGame.EndGame
}

// Copy of server-utils' eponymous function
static std::string getConfigPath() {
	static char* pathStr = nullptr;
	if (pathStr == nullptr) {
		std::string path = Utils::FS::BlreviveConfigPath();
		pathStr = new char[path.length() + 1];
		strcpy(pathStr, path.c_str());
		return path;
	}
	else {
		return std::string(pathStr);
	}
}

// Copy of server-utils' eponymous function
static std::string getOutputPath() {
	std::string outputPath = getConfigPath() + "stats_uploader/";

	struct stat info;
	if (stat(outputPath.c_str(), &info) == 0) {
		// path exists
		if (info.st_mode & S_IFDIR) {
			// path exists and is a dir
			return outputPath;
		}
		else {
			Log->error(fmt::format("{0} exists but it is not a directory", outputPath));
			return "";
		}
	}
	else {
		// path do not exist
		if (CreateDirectory(outputPath.c_str(), nullptr)) {
			return outputPath;
		}
		else {
			Log->error(fmt::format("cannot create directory {0}", outputPath));
			return "";
		}
	}
}

// Copy of server-utils' eponymous function
static void writeConfig(nlohmann::json& toWrite, std::string path) {
	std::ofstream output(path);
	if (!output.is_open()) {
		Log->error(fmt::format("failed writing config to {0}", path));
		return;
	}
	output << toWrite.dump(4) << std::endl;
	output.close();
	return;
}

// Copy of server-utils' eponymous function
static nlohmann::json defaultConfigJson() {
	nlohmann::json defaultConfig;

	defaultConfig["Game"]["Modded"] = false;

	defaultConfig["Api"]["URL"] = "undefined";
	defaultConfig["Api"]["Token"] = "undefined";
	defaultConfig["Api"]["Secure"] = false;

	return defaultConfig;
}

// Copy of server-utils' eponymous function
static nlohmann::json getJsonValue(nlohmann::json& input, nlohmann::json& defaultOverlay, std::string category, std::string param) {
	nlohmann::json val;
	try {
		val = input[category][param];
		if (val.is_null()) {
			Log->warn(fmt::format("{0}/{1} not found in config, using default value", category, param));
			val = defaultOverlay[category][param];
		}
	}
	catch (nlohmann::json::exception e) {
		Log->error("Failed getting json value:");
		Log->error(e.what());
		throw(e);
	}
	return val;
}

// Copy of server-utils' serverConfigFromJson function
static UploaderConfig uploaderConfigFromJson(nlohmann::json input) {
	UploaderConfig config;

	nlohmann::json defaultConfig = defaultConfigJson();
	nlohmann::json val;
	try {
		config.Game.Modded = (bool)getJsonValue(input, defaultConfig, "Game", "Modded");

		config.Api.URL = getJsonValue(input, defaultConfig, "Api", "URL");
		config.Api.Token = getJsonValue(input, defaultConfig, "Api", "Token");
		config.Api.Secure = (bool)getJsonValue(input, defaultConfig, "Api", "Secure");
	}
	catch (nlohmann::json::exception e) {
		Log->error("Failed parsing config json:");
		Log->error(e.what());
		throw(e);
	}
	return config;
}

// Copy of server-utils' serverConfigFromFile function
static UploaderConfig uploaderConfigFromFile() {
	std::string outputPath = getOutputPath();
	if (outputPath.length() == 0) {
		Log->error(fmt::format("cannot load config from {0}", outputPath));
		return uploaderConfigFromJson(defaultConfigJson());
	}

	std::string serverConfigPath = fmt::format("{0}{1}", outputPath, "stats_uploader.json");
	std::ifstream input(serverConfigPath);
	if (!input.is_open()) {
		Log->debug(fmt::format("{0} does not exist, writing default config", serverConfigPath));
		nlohmann::json config = defaultConfigJson();
		writeConfig(config, serverConfigPath);
		return uploaderConfigFromJson(config);
	}

	try {
		nlohmann::json inputJson = nlohmann::json::parse(input);
		input.close();
		return uploaderConfigFromJson(inputJson);
	}
	catch (nlohmann::json::exception e) {
		Log->error(fmt::format("failed parsing {0}, using default config", serverConfigPath));
		Log->error(e.what());
		input.close();
		return uploaderConfigFromJson(defaultConfigJson());
	}
}

// Copy of server-utils' eponymous function
static std::string unrealStringToString(FString& value) {
	if (value.Data == NULL) {
		return std::string("");
	}
	const char* cstr = value.ToChar();
	std::string ret = std::string(cstr);
	free((void*)cstr);
	return ret;
}

// Generate and return an ISO8601 timestamp
static std::string getIso8601Timestamp()
{
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t currentTime = std::chrono::system_clock::to_time_t(now);

	std::tm* localTime = std::localtime(&currentTime);
	std::stringstream isoSS;
	isoSS << std::put_time(localTime, "%FT%T%z");

	std::string isoTimestamp = isoSS.str();

	return isoTimestamp;
}

static void writeEndgameJson(nlohmann::json game_json) {
	std::string outputPath = getOutputPath();
	if (outputPath.length() == 0) {
		Log->error("Cannot write game_result.json, destination is inaccessible");
		return;
	}
	std::string path = fmt::format("{0}{1}", outputPath, "game_result.json");

	try
	{
		std::ofstream output(path);
		if (!output.is_open()) {
			Log->error(fmt::format("Failed writing server info to {0}", path));
			return;
		}

		std::string outputString = game_json.dump(4);
		output << outputString << std::endl;
		output.close();
	}
	catch (std::exception e)
	{
		Log->error(fmt::format("Failed saving {0}", path));
		Log->error(e.what());
	};
}

// Pretty much a copy of server-utils' updateServerInfo with a tweaked JSON schema
static nlohmann::json getEndgameJson(AFoxGame* game, GameProperties game_properties)
{
	nlohmann::json gameInfo;

	try {
		gameInfo["timestamp"] = getIso8601Timestamp();
		gameInfo["modded"] = game_properties.Modded;

		std::string gameType = unrealStringToString(game->GameTypeAbbreviatedName);
		gameInfo["gamemode"] = gameType;

		FString mapNameFString = game->WorldInfo->GetMapName(true);
		std::string mapName = unrealStringToString(mapNameFString);
		gameInfo["map"] = mapName;

		TArray<AFoxTeamInfo*> teams = game->Teams;
		TArray<APlayerReplicationInfo*> players = game->GameReplicationInfo->PRIArray;

		gameInfo["teams"] = nlohmann::json::array();
		// there seems to always be a dummy team, said Katie. Not gonna contest it
		for (int i = 0; i < (game->NumTeams - 1) && i < (teams.Count - 1); i++) {
			AFoxTeamInfo* team = teams.at(i);
			gameInfo["teams"][i]["players"] = nlohmann::json::array();
			int playerIndex = 0;
			for (int j = 0; j < players.Count; j++)
			{
				AFoxPRI* player = (AFoxPRI*)players.at(j);
				if ((void*)team == (void*)player->Team) {
					gameInfo["teams"][i]["players"][playerIndex]["name"] = unrealStringToString(player->PlayerName);
					gameInfo["teams"][i]["players"][playerIndex]["kills"] = player->TotalKills;
					gameInfo["teams"][i]["players"][playerIndex]["deaths"] = player->TotalDeaths;
					gameInfo["teams"][i]["players"][playerIndex]["score"] = player->TotalEarnedXP;
					gameInfo["teams"][i]["players"][playerIndex]["bot"] = bool(player->bBot);
					playerIndex++;
				}
			}
		}

		writeEndgameJson(gameInfo);

		return gameInfo;
	}
	catch (nlohmann::json::exception e) {
		Log->error("Failed parsing game data for upload:");
		Log->error(e.what());
		nlohmann::json empty;
		return empty;
	}
}

// Callback for curl request
size_t WriteCallback(void* contents, size_t size, size_t nmemb, std::string* output) {
	size_t totalSize = size * nmemb;
	output->append(static_cast<char*>(contents), totalSize);
	return totalSize;
}

// Uploads stats json to API
static void uploadLeaderboard(AFoxGame* game, UploaderConfig uploaderConfig, nlohmann::json endgameJson)
{
	CURL* curl;
	CURLcode res;

	curl = curl_easy_init();

	if (curl) {
		std::string url = uploaderConfig.Api.URL;
		if (url.back() != '/') {
			url += '/';
		}
		url = url + "api/v1/games/";
		Log->debug(fmt::format("Preparing PUT query to endpoint {}", url));

		struct curl_slist* headers = NULL;
		headers = curl_slist_append(headers, "Content-Type: application/json");
		headers = curl_slist_append(headers, fmt::format("Authorization: Bearer {}", uploaderConfig.Api.Token).c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "BLRE Stats-Uploader/v1");
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
		// Just in case I ever want to follow redirects
		// curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		std::string json_string = endgameJson.dump();

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_string.c_str());

		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

		if (uploaderConfig.Api.Secure) {
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
		}

		std::string response;
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		res = curl_easy_perform(curl);

		curl_easy_cleanup(curl);

		curl_slist_free_all(headers);

		if (res != CURLE_OK) {
			Log->error(fmt::format("CURL request failed: {}", curl_easy_strerror(res)));
			return;
		}

		Log->info(fmt::format("Posted game results, got reply from API: {}", response));
		return;
	}
}
