#pragma once

#include <BLRevive/BLRevive.h>

// static reference to the BLRevive API 
static BLRE::BLRevive* BLReviveAPI;

// logger instance of this module
static BLRE::LogFactory::TSharedLogger Log;

struct GameProperties {
    bool Modded;
};

struct ApiEndpoint {
    std::string URL;
    std::string Token;
    bool Secure;
};

struct UploaderConfig {
    GameProperties Game;
    ApiEndpoint Api;
};

static UploaderConfig uploaderConfigFromFile();
static std::string getIso8601Timestamp();
static nlohmann::json getEndgameJson(AFoxGame* game, GameProperties game_properties);
static void uploadLeaderboard(AFoxGame* game, UploaderConfig uploaderConfig, nlohmann::json endgameJson);